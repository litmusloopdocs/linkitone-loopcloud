#include <LWiFi.h>
#include <LWiFiClient.h>
#include <loopmq.h>
#include <stdio.h>
#include <ArduinoJson.h> 
#include <ADXL345.h>
#include <Wire.h>
#include "configuration.h"

/*
  Passing the defined information to char
*/
int port = port_number;                       // port number
char hostname[] = server;                     // Server name
char c[] = clientID;                        // ClientID
char pass[]= password;                      // password
char user[] = userID;                       // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device 

/* 
 constants won't change. They're used here to
 set pin numbers:
*/ 



const int TouchPin=8;
const int lightpin=A1;



String ledStatus;
float Rsensor; //Resistance of sensor in K

ADXL345 adxl; //variable adxl is an instance of the ADXL345 library

LWiFiClient wificlient;                         // client instance to connect to wifi
PubSubClient loopmq(wificlient);               // instance to use the loopmq functions.

/*
 Function to calculate the LED Status

String ledstatus(){
  if(digitalRead(ledPin) == HIGH)
  return ledStatus = "ON";
  else
  return ledStatus = "OFF";
}
*/

void InitLWiFi()
{
  LWiFi.begin();
  // Keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD))) {
    delay(1000);
  }
  Serial.println("Connected to AP");
}

void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
/*
  Display the message published serially and on the LCD
*/
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);           // Serial Display
  }
     Serial.println();                         //Print on a new line
}

void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");    // Attempt to connect    
    if (loopmq.connect(c,user,pass)) {
    Serial.println("connected");            // Once connected, publish an announcement...
      
         loopmq.subscribe(s);                 // Subscribe to a topic  
/*  
  To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   
/*
  To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(2000);                            // Wait 5 seconds before retrying
    }
  }
}

void setup()
{
   
   Serial.begin(9600);
   InitLWiFi();
    
  pinMode(TouchPin,INPUT);
  pinMode(lightpin,INPUT);
  
  
  adxl.powerOn();

  //set activity/ inactivity thresholds (0-255)
  adxl.setActivityThreshold(75); //62.5mg per increment
  adxl.setInactivityThreshold(75); //62.5mg per increment
  adxl.setTimeInactivity(10); // how many seconds of no activity is inactive?
 
  //look of activity movement on this axes - 1 == on; 0 == off 
  adxl.setActivityX(1);
  adxl.setActivityY(1);
  adxl.setActivityZ(1);
 
  //look of inactivity movement on this axes - 1 == on; 0 == off
  adxl.setInactivityX(1);
  adxl.setInactivityY(1);
  adxl.setInactivityZ(1);
 
  //look of tap movement on this axes - 1 == on; 0 == off
  adxl.setTapDetectionOnX(0);
  adxl.setTapDetectionOnY(0);
  adxl.setTapDetectionOnZ(1);
 
  //set values for what is a tap, and what is a double tap (0-255)
  adxl.setTapThreshold(50); //62.5mg per increment
  adxl.setTapDuration(15); //625us per increment
  adxl.setDoubleTapLatency(80); //1.25ms per increment
  adxl.setDoubleTapWindow(200); //1.25ms per increment
 
  //set values for what is considered freefall (0-255)
  adxl.setFreeFallThreshold(7); //(5 - 9) recommended - 62.5mg per increment
  adxl.setFreeFallDuration(45); //(20 - 70) recommended - 5ms per increment
 
  //setting all interrupts to take place on int pin 1
  //I had issues with int pin 2, was unable to reset it
  adxl.setInterruptMapping( ADXL345_INT_SINGLE_TAP_BIT,   ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_DOUBLE_TAP_BIT,   ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_FREE_FALL_BIT,    ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_ACTIVITY_BIT,     ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_INACTIVITY_BIT,   ADXL345_INT1_PIN );
 
  //register interrupt actions - 1 == on; 0 == off  
  adxl.setInterrupt( ADXL345_INT_SINGLE_TAP_BIT, 1);
  adxl.setInterrupt( ADXL345_INT_DOUBLE_TAP_BIT, 1);
  adxl.setInterrupt( ADXL345_INT_FREE_FALL_BIT,  1);
  adxl.setInterrupt( ADXL345_INT_ACTIVITY_BIT,   1);
  adxl.setInterrupt( ADXL345_INT_INACTIVITY_BIT, 1);
  
  
  loopmq.setServer(hostname                              , port);           // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published 
  
  

   delay(2000);                                // Allow the hardware to sort itself out
}

void loop(){

/*
  Sensor Code for Touch Sensor and LED
*/
  int sensorValue = digitalRead(TouchPin);
  Serial.print(sensorValue);
  
  
/*
 LIGHT SENSOR CODE
*/
  int sensorval = analogRead(lightpin); 
  Rsensor=(float)(1023-sensorval)*10/sensorval;
  Serial.print(sensorval);
  Serial.print(',');
  Serial.print(Rsensor);
  

/*
 Sensor Code for UV sensor

  int uv_sensorValue;
  long  sum=0;
  for(int i=0;i<1024;i++)// accumulate readings for 1024 times
   {  
      uv_sensorValue=analogRead(uvpin);
      sum=uv_sensorValue+sum;
      delay(2);
   }   
 long meanVal = sum/1024;  // get mean value
 float UV_index = (meanVal*1000/4.3-83)/21 ;
 Serial.print("The current UV index is:");
 Serial.print(UV_index);// get a detailed calculating expression for UV index in schematic files. 
 Serial.print("\n");
*/

/*
   ACCELEROMETER SENSOR CODE
   */
	int x,y,z;  
	adxl.readXYZ(&x, &y, &z); //read the accelerometer values and store them in variables  x,y,z
	// Output x,y,z values 
	Serial.print("values of X , Y , Z: ");
	Serial.print(x);
	Serial.print(" , ");
	Serial.print(y);
	Serial.print(" , ");
	Serial.println(z);
	
	double xyz[3];
	double ax,ay,az;
	adxl.getAcceleration(xyz);
	ax = xyz[0];
	ay = xyz[1];
	az = xyz[2];
	Serial.print("X=");
	Serial.print(ax);
    Serial.println(" g");
	Serial.print("Y=");
	Serial.print(ay);
    Serial.println(" g");
	Serial.print("Z=");
	Serial.println(az);
    Serial.println(" g");
	Serial.println("**********************");
	delay(500);

/*  
 JSON parser
*/
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Sensors with Linkit one";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Touch Sensor"]= sensorValue;                                // Add data["key"]= value
  data["Immuminance"]= Rsensor;
  data["X"]= ax;
  data["Y"]= ay;
  data["Z"]= az;
  

  root.printTo(Serial);                           // prints to serial terminal
  Serial.println();

  char buffer[200];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload 
/*
  Publish to the server
*/
  
  if (!loopmq.connected()) {
    reconnect();                              // Try to reconnect if connection dropped
 }
  if (loopmq.connect(c, user, pass)){
 
   loopmq.publish(p,buffer);                     // Publish message to the server once only
     delay(500);
 }
  
  loopmq.loop();                              // check if the network is connected and also if the client is up and running  
}


