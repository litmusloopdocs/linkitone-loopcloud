# all_linkint_sensors

This project is to demonstrate the implementation of MQTT. **Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.

**Touch Sensor** enables you to replace press with touch. It can detect the change in capacitance when a finger is near by. That means no matter your finger directly touches the pad or just stays close to the pad, Touch Sensor would outputs HIGH also.
[Click here to find more information about the Touch Sensor.](http://www.seeedstudio.com/wiki/Grove_-_Touch_Sensor)

The **Accelerometer Sensor** is a high resolution digital accelerometer providing you at max 3.9mg/LSB resolution and large ±16g measurement range. It's base on an advanced 3-axis IC ADXL345. Have no worry to implement it into your free-fall detection project, cause it's robust enough to survive up to 10,000g shock. Meanwhile, it's agile enough to detect single and double taps. It's ideal for motion detection, Gesture detection as well as robotics.This is a high resolution digital accelerometer providing you at max 3.9mg/LSB resolution and large ±16g measurement range. It's base on an advanced 3-axis IC ADXL345. Have no worry to implement it into your free-fall detection project, cause it's robust enough to survive up to 10,000g shock. Meanwhile, it's agile enough to detect single and double taps. It's ideal for motion detection, Gesture detection as well as robotics.[Click here to find more information about Accelerometer Sensor](http://wiki.seeedstudio.com/wiki/Grove_-_3-Axis_Digital_Accelerometer(%C2%B116g))

The **Light Sensor** module incorporates a Light Dependent Resistor (LDR). Typically, the resistance of the LDR or Photoresistor will decrease when the ambient light intensity increases. This means that the output signal from this module will be HIGH in bright light, and LOW in the dark.
[Click here to find more information about the light sensor. ](http://www.seeedstudio.com/wiki/Grove_-_Light_Sensor)


## Overview


The library provides an example of publish and subscribe messaging with a server that supports MQTT using Linkit One.
 
***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.


***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started With Linkit one


**Basic steps to connect Linkit to internet are given below:**

>1. Download Linkit one from Board's Manager as per the instructions given on the [link](https://labs.mediatek.com/site/global/developer_tools/mediatek_linkit/get-started/windows_os_stream/get_hardware_software/index.gsp)
2. Connect Linkit one Wi-Fi antenna
3. Include "LWiFiClient.h" and "LWiFi.h" and define the below defination to connect to the Wi-Fi.

	```
	void InitLWiFi()
		{
		  LWiFi.begin();
		  // Keep retrying until connected to AP
		  Serial.println("Connecting to AP");
		  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD))) {
			delay(1000);
		  }
		  Serial.println("Connected to AP");
		}
	```

**Steps to connect the board and then send data to MQTT are as below:**

>1. Assemble and connect the board using micro USB cable
2. Install Arduino IDE and select YUN from boards and the COM port it is connected to
3. Install the library or open the *all_linkitone.ino* file from the examples.
4. Enter the MQTT broker details in the *configuration.h* file present along with the *.ino* file.
5. Once code is compiled and flashed to linkitone, you should see the messages published by you to the device. 

**Steps to test MQTT connection (if required) can be found under /repo/extras/testmqqt.md :**

***Note***: *If you are not using Google Chrome as your default browser, download **MQTTSpy** to test MQTT connection.*


## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

**Below are the list of minimum definitions required by the user to send data to the cloud:**

```
#define WIFI_AP "LitmusAutomation-Internal"           // SSID name
#define WIFI_PASSWORD "Zxcvbn<>890"		      // Wi-Fi password
#define WIFI_AUTH LWIFI_WPA  			      // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
#define port_number 1883                              // Port number
#define server "liveiot.mq.litmusloop.com"            // Server name
#define clientID "linkitoneclient"                    // ClientID
#define password "8Hgw*j#978pT"                       // password
#define userID "demoroom"                             // username 
#define subTOPIC "demoroom/linkit/allpub"             // Subscribe on this topic to get the data
#define pubTopic "demoroom/linkit/allsub"             // Publish on this tpoic to send data or command to device 
```

## Functions

1.***loopmq.connect (client ID)***

This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected");
```       


2.***loopmq.connect (client ID, username, password)***

Checks for the username and password specified by the user to connect the device to the network.

```
if (loopmq.connect(c, user, pass))
  loopmq.publish(p,buffer);                 // Publish message to the server once only
```
3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 

```
Loopmq.publish (p,buffer);                   // Publish message to the server
```

4.***loopmq.subscribe (topic)***

This function is used to subscribe to a topic to which data will be published from the user to the     device. 

```
loopmq.subscribe(s);                          // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***

This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);                     // Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();                       // Note: uncomment the code to disconnect the device
```
7.***loop ()***

This function is the sensor code for the Touch,accelerometer,light sensor.

```
/*
  Sensor Code for Touch Sensor and LED
*/
  int sensorValue = digitalRead(TouchPin);
  Serial.print(sensorValue);
  
  
/*
 LIGHT SENSOR CODE
*/
  int sensorval = analogRead(lightpin); 
  Rsensor=(float)(1023-sensorval)*10/sensorval;
  Serial.print(sensorval);
  Serial.print(',');
  Serial.print(Rsensor);
  

/*
 Sensor Code for UV sensor

  int uv_sensorValue;
  long  sum=0;
  for(int i=0;i<1024;i++)// accumulate readings for 1024 times
   {  
      uv_sensorValue=analogRead(uvpin);
      sum=uv_sensorValue+sum;
      delay(2);
   }   
 long meanVal = sum/1024;  // get mean value
 float UV_index = (meanVal*1000/4.3-83)/21 ;
 Serial.print("The current UV index is:");
 Serial.print(UV_index);// get a detailed calculating expression for UV index in schematic files. 
 Serial.print("\n");
*/

/*
   ACCELEROMETER SENSOR CODE
   */
	int x,y,z;  
	adxl.readXYZ(&x, &y, &z); //read the accelerometer values and store them in variables  x,y,z
	// Output x,y,z values 
	Serial.print("values of X , Y , Z: ");
	Serial.print(x);
	Serial.print(" , ");
	Serial.print(y);
	Serial.print(" , ");
	Serial.println(z);
	
	double xyz[3];
	double ax,ay,az;
	adxl.getAcceleration(xyz);
	ax = xyz[0];
	ay = xyz[1];
	az = xyz[2];
	Serial.print("X=");
	Serial.print(ax);
    Serial.println(" g");
	Serial.print("Y=");
	Serial.print(ay);
    Serial.println(" g");
	Serial.print("Z=");
	Serial.println(az);
    Serial.println(" g");
	Serial.println("**********************");
	delay(500);

```

8.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Sensors with Linkit one";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Touch Sensor"]= sensorValue;                                // Add data["key"]= value
  data["Immuminance"]= Rsensor;
  data["X"]= ax;
  data["Y"]= ay;
  data["Z"]= az;
  

  root.printTo(Serial);                           // prints to serial terminal
  Serial.println();

  char buffer[200];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload 
  
```